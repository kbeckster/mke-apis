import axios from 'axios';

var axiosInstance = axios.create({
	baseURL: "https://transloc-api-1-2.p.rapidapi.com",
	timeout: 10000,
	headers: {
		'X-RapidAPI-Key': '<GET KEY FROM SIGN UP>',
		'X-RapidAPI-Host': 'transloc-api-1-2.p.rapidapi.com'
	}
});

// Represents the hop ID
const angency = "1425";

// Represents the one route Milwaukee currently has (need to update if they add more routes)
const route = "8005754";

/**
 * Finds the stops near the location within a radius
 * @param {string} latitude of user
 * @param {string} longitude of user
 * @param {number} radius to search for stops
 * @returns stops in format {name: '', id: '', latitude: '', longitude: '' }
 */
export async function getHopStops() {
	var allStops = await axiosInstance.get(`/stops.json?agencies=${angency}&routes=${route}`);
	var stopsFormated = allStops.data.data.map(x => {
		return {
			latitude: x.location.lat,
			longitude: x.location.lng,
			id: x.stop_id,
			name: x.name
		}
	});

	return stopsFormated;
}

/**
 * Find estimated arrivals of next street car to a stop 
 * @param {string} stopId
 * @returns list of next estimated arrivals in Date object. Format is "yyyy'-'MM'-'dd'T'HH':'mm':'ss'
 */
export async function getArrivalEstimates(stopId) {
	return axiosInstance.get(`arrival-estimates.json?agencies=${angency}&routes=${route}&stops=${stopId}`).then(res => {
		return res.data.data.map(x => x.arrivals.map(y => new Date(y.arrival_at))).reduce((x, y) => x.concat(y), []);
	});
}