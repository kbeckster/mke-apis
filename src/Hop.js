import React, { useState, useEffect } from 'react';
import { getHopStops } from './actions/hop-api';

function Hop() {

  const [hopStops, setHopStops] = useState([]);

  useEffect(() => {
      getData();
  }, []);

  const getData = async () => {
    const hopStops =  await getHopStops();
    setHopStops(hopStops);
  }
    return (
        <div >
          <h3>Milwaukee Hop Locations by Name</h3>
          {hopStops.map((stop, index) => (
            <div key={index}>{stop.name}</div>
          ))}
        </div>
    );
}

export default Hop;

